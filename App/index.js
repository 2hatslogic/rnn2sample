import { Navigation } from "react-native-navigation";

import Home from './screens/Home';
import About from './screens/About';
import Profile from './screens/Profile';

Navigation.registerComponent('app.HomeScreen', () => Home);
Navigation.registerComponent('app.AboutScreen', () => About);
Navigation.registerComponent('app.ProfileScreen', () => Profile);

Navigation.setRoot({
  root: {
    sideMenu: {
      left: {
        component: {
          name: 'app.ProfileScreen',
        },
      },
      center: {
        bottomTabs: {
          children: [
            {
              component: {
                name: 'app.AboutScreen',
                options: {
                  bottomTab: {
                    fontSize: 12,
                    textColor: '#85c1e9',
                    iconColor: '#85c1e9',
                    selectedIconColor: '#1b4f72',
                    selectedTextColor: '#1b4f72',
                    text: 'Home',
                    icon: require('./assets/icon/profileIcon.png')
                  }
                }
              }
            },

            {
              component: {
                name: 'app.HomeScreen',
                options: {
                  bottomTab: {
                    fontSize: 12,
                    textColor: '#85c1e9',
                    iconColor: '#85c1e9',
                    selectedIconColor: '#1b4f72',
                    selectedTextColor: '#1b4f72',
                    text: 'About',
                    icon: require('./assets/icon/profileIcon.png')
                  }
                }
              }
            }
          ],
        }
      }
    }
  }
});